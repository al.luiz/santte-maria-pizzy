﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SantteMaria.Pizzy.Pedidos
{
    public class Item
    {
        public Item(string nome, decimal preco)
        {
            Nome = nome;
            Preco = preco;
            Id = new Random().Next();
        }

        public int Id { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string Observacoes { get; set; }
        public decimal Preco { get; set; }

        public override bool Equals(object obj)
        {
            Item item2 = (Item)obj;

            return this.Id == item2.Id;
        }
    }
}
