﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace SantteMaria.Pizzy.Pedidos
{
    public class Contato
    {
        public Telefone Telefone { get; set; }
        public MailAddress Email { get; set; }
        public Endereco Endereco { get; set; }
    }
}
