﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SantteMaria.Pizzy.Pedidos
{
    public abstract class Pessoa
    {
        protected Pessoa(string nome)
        {
            Nome = nome;
        }

        public int Id { get; set; }
        public string Nome { get; set; }
        public Contato Contato { get; set; }
    }
}
