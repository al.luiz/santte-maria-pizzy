﻿using System;
using System.Collections.Generic;

namespace SantteMaria.Pizzy.Pedidos
{
    public enum StatusPedido
    {
        Aberto,
        Solicitado,
        Preparando,
        Pronto,
        EmEntrega,
        Finalizado,
        Cancelado
    }

    public class Pedido
    {
        /// <summary>
        /// Criar um novo pedido.
        /// </summary>
        public Pedido(Colaborador solicitante, Cliente cliente)
        {
            Solicitante = solicitante;
            Cliente = cliente;
            Itens = new Dictionary<Item, int>();
            HoraInicio = DateTime.Now;
            Status = StatusPedido.Aberto;
        }

        //public int Id { get; set; }

        public Colaborador Solicitante { get; private set; }
        public Cliente Cliente { get; private set; }
        public DateTime HoraInicio { get; private set; }
        public DateTime HoraFim { get; set; }

        public Colaborador Executor { get; set; }
        public Colaborador Entregador { get; set; }
        public decimal Valor { get; set; }
        public Dictionary<Item, int> Itens { get; private set; }
        public StatusPedido Status { get; set; }
        public bool HouveEstorno { get; set; }
        public decimal Estorno { get; internal set; }
    }
}
