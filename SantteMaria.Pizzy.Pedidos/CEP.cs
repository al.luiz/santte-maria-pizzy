﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace SantteMaria.Pizzy.Pedidos
{
    public class CEP
    {
        public CEP(string cep)
        {
            Regex regex = new Regex(@"^\d{5}-{0,1}\d{3}");

            if ((cep.Length == 8 || cep.Length == 9) && regex.IsMatch(cep))
            {
                cep = cep.Replace("-", "");
                Valor = cep;
            }
            else
                throw new ArgumentException("CEP Invalido", "cep");
        }

        public string Valor { get; private set; }
    }
}
