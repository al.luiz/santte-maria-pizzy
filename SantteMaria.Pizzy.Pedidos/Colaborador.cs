﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SantteMaria.Pizzy.Pedidos
{
    public enum Cargo
    {
        Operador,
        Pizzaiolo,
        Gerente
    }

    public class Colaborador: Pessoa
    {
        public Colaborador(string nome): base(nome)
        {
        }

        public Cargo Cargo { get; set; }
    }
}
