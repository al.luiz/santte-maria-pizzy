﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SantteMaria.Pizzy.Pedidos
{
    public class PedidoService
    {
        /// <summary>
        /// Criar um novo pedido
        /// </summary>
        /// <returns>Um novo pedido</returns>
        /// <param name="solicitante">Colaborador solicitante do pedido</param>
        /// <param name="cliente">Cliente que fez o pedido</param>
        public Pedido NovoPedido(Colaborador solicitante, Cliente cliente)
        {
            ValidarColaborador(solicitante);
            ValidarCliente(cliente);

            return new Pedido(solicitante, cliente);
        }

        /// <summary>
        /// Validar se o cliente é válido
        /// </summary>
        /// <param name="cliente">O cliente do pedido</param>
        private static void ValidarCliente(Cliente cliente)
        {
            if (cliente == null)
                throw new ArgumentNullException("cliente", PedidoServiceMessages.MSG_CAMPO_NULO);
        }

        /// <summary>
        /// Adicionar itens ao pedido.
        /// </summary>
        /// <param name="pedido">Pedido que receberá o item</param>
        /// <param name="item">Item a ser adicionado</param>
        public void IncluirItem(Pedido pedido, Item item)
        {
            try
            {
                ValidarPedidoEItem(pedido, item);

                if (pedido.Itens.ContainsKey(item))
                    throw new ArgumentException(PedidoServiceMessages.MSG_ITEM_EXISTENTE, "item");

                pedido.Itens.Add(item, 1);

                Calcular(pedido);

            }
            catch (ArgumentException)
            {
                throw;
            }
        }

        /// <summary>
        /// Validar o pedido e o item
        /// </summary>
        /// <param name="pedido">Pedido a ser validado</param>
        /// <param name="item">Item a ser validado</param>
        private static void ValidarPedidoEItem(Pedido pedido, Item item)
        {
            ValidarPedido(pedido);
            ValidarItem(item);

            if (pedido.Status != StatusPedido.Aberto && pedido.Status != StatusPedido.Solicitado)
                throw new ArgumentException(PedidoServiceMessages.MSG_PEDIDO_INICIADO, "pedido");
        }

        /// <summary>
        /// Validar o item
        /// </summary>
        /// <param name="item">Item a ser validado</param>
        private static void ValidarItem(Item item)
        {
            if (item == null)
                throw new ArgumentNullException("item", PedidoServiceMessages.MSG_CAMPO_NULO);
        }

        /// <summary>
        /// Validar o pedido
        /// </summary>
        /// <param name="pedido">Pedido a ser validado</param>
        private static void ValidarPedido(Pedido pedido)
        {
            if (pedido == null)
                throw new ArgumentNullException("pedido", PedidoServiceMessages.MSG_CAMPO_NULO);

        }

        /// <summary>
        /// Incrementar/Decrementar quantidade de um determinado item do pedido.
        /// </summary>
        /// <param name="pedido">Pedido que receberá o item</param>
        /// <param name="item">Item a ser incrementado/decrementado</param>
        /// <param name="quantidade">Quantidade do item</param>
        public void AtualizarItem(Pedido pedido, Item item, int quantidade)
        {
            ValidarPedidoEItem(pedido, item);

            if (pedido.Itens.ContainsKey(item))
            {
                if (quantidade > 0)
                    pedido.Itens[item] = quantidade;
                else
                    throw new ArgumentOutOfRangeException("quantidade", PedidoServiceMessages.MSG_QTD_MIN);

                Calcular(pedido);
            }
            else
            {
                throw new ArgumentException(PedidoServiceMessages.MSG_ITEM_INEXISTENTE, "item");
            }
        }

        /// <summary>
        /// Remover ítem do pedido.
        /// </summary>
        /// <param name="pedido">Pedido a que o item será retirado</param>
        /// <param name="item">Item a ser retirado</param>
        public void RemoverItem(Pedido pedido, Item item)
        {
            ValidarPedidoEItem(pedido, item);

            if (pedido.Itens.ContainsKey(item))
            {
                pedido.Itens.Remove(item);
                Calcular(pedido);
            }
            else
            {
                throw new ArgumentException(PedidoServiceMessages.MSG_ITEM_INEXISTENTE);
            }
        }

        /// <summary>
        /// Calcular o valor do pedido. Realiza o cálculo do preço com base nos itens e quantidade apenas para informação prévia ao cliente.
        /// </summary>
        /// <param name="pedido">Pedido a ser calculado</param>
        public void Calcular(Pedido pedido)
        {
            ValidarPedido(pedido);

            decimal subtotal = 0;

            foreach (KeyValuePair<Item, int> keyValue in pedido.Itens)
            {
                int quantidadeItem = keyValue.Value;
                decimal valorItem = quantidadeItem * keyValue.Key.Preco;

                subtotal += valorItem;
            }

            pedido.Valor = subtotal;
        }

        /// <summary>
        /// Solicitar o pedido. Realiza o cálculo do preço com base nos itens e quantidade, além disso altera o status do pedido para Solicitado.
        /// </summary>
        /// <param name="pedido">Pedido a ser solicitado</param>
        public void Solicitar(Pedido pedido)
        {
            ValidarPedido(pedido);

            if (pedido.Itens.Count > 0)
            {
                if (pedido.Status == StatusPedido.Aberto)
                {
                    Calcular(pedido);
                    pedido.Status = StatusPedido.Solicitado;
                }
                else
                {
                    throw new ArgumentException(PedidoServiceMessages.MSG_PEDIDO_ABERTO, "pedido");
                }
            }
            else
            {
                throw new ArgumentException(PedidoServiceMessages.MSG_QTD_MIN_ITEM, "pedido");
            }
        }

        /// <summary>
        /// Preparar o pedido. Altera o status do pedido para Preparando.
        /// </summary>
        /// <param name="pedido">Pedido a ser preparado</param>
        /// <param name="executor">Responsável por executar o pedido</param>
        public void Preparar(Pedido pedido, Colaborador executor)
        {
            ValidarPedido(pedido);
            ValidarColaborador(executor);

            if (pedido.Status == StatusPedido.Solicitado)
            {
                pedido.Status = StatusPedido.Preparando;
                pedido.Executor = executor;
            }
            else
                throw new ArgumentException(PedidoServiceMessages.MSG_PEDIDO_SOLICITADO, "pedido");
        }

        /// <summary>
        /// Validar o colaborador
        /// </summary>
        /// <param name="colaborador">O colaborador a ser validado</param>
        private void ValidarColaborador(Colaborador colaborador)
        {
            if (colaborador == null)
                throw new ArgumentNullException("colaborador", PedidoServiceMessages.MSG_CAMPO_NULO);
        }

        /// <summary>
        /// Pedido pronto. Altera o status do pedido para pronto.
        /// </summary>
        /// <param name="pedido">Pedido pronto</param>
        public void Pronto(Pedido pedido)
        {
            ValidarPedido(pedido);

            if (pedido.Status == StatusPedido.Preparando)
                pedido.Status = StatusPedido.Pronto;
            else
                throw new ArgumentException(PedidoServiceMessages.MSG_PEDIDO_PRONTO, "pedido");
        }

        /// <summary>
        /// Entregar o pedido. Altera o status do pedido para EmEntrega.
        /// </summary>
        /// <param name="pedido">Pedido a ser entregue</param>
        /// <param name="entregador">O entregador do pedido</param>
        public void Entregar(Pedido pedido, Colaborador entregador)
        {
            ValidarPedido(pedido);
            ValidarColaborador(entregador);

            if (pedido.Status == StatusPedido.Pronto)
            {
                pedido.Status = StatusPedido.EmEntrega;
                pedido.Entregador = entregador;
            }
            else
                throw new ArgumentException(PedidoServiceMessages.MSG_PEDIDO_ENTREGAR, "pedido");
        }

        /// <summary>
        /// Pedido entregue. Altera o status do pedido para Finalizado.
        /// </summary>
        /// <param name="pedido">Pedido a ser finalizado</param>
        public void Finalizar(Pedido pedido)
        {
            ValidarPedido(pedido);

            if (pedido.Status == StatusPedido.EmEntrega)
            {
                pedido.Status = StatusPedido.Finalizado;
                pedido.HoraFim = DateTime.Now;
            }
            else
                throw new ArgumentException(PedidoServiceMessages.MSG_PEDIDO_FINALIZADO, "pedido");
        }

        /// <summary>
        /// Estornar o valor parcial do pedido. Para estorno total use o cancelamento.
        /// </summary>
        /// <param name="pedido">Pedido a ser estornado</param>
        /// <param name="estorno">Valor do estorno</param>
        public void EstornoParcial(Pedido pedido, decimal estorno)
        {
            ValidarPedido(pedido);

            if (pedido.Status == StatusPedido.Finalizado)
            {
                if (estorno > 0 && pedido.Valor >= estorno)
                {
                    if (pedido.Valor > estorno)
                    {
                        Estornar(pedido, estorno);
                    }
                    else
                        throw new ArgumentOutOfRangeException("estorno", PedidoServiceMessages.MSG_PEDIDO_ESTORNO_TOTAL);
                }
                else
                    throw new ArgumentOutOfRangeException("estorno", PedidoServiceMessages.MSG_ESTORNO_VALOR);
            }
            else if (pedido.HouveEstorno)
                throw new ArgumentException(PedidoServiceMessages.MSG_PEDIDO_ESTORNO_DUPLO, "pedido");
            else
                throw new ArgumentException(PedidoServiceMessages.MSG_PEDIDO_ESTORNO, "pedido");
        }

        /// <summary>
        /// Estornar o valor.
        /// </summary>
        /// <param name="pedido">Pedido que terá o estorno</param>
        /// <param name="estorno">Valor do estorno</param>
        private void Estornar(Pedido pedido, decimal estorno)
        {
            pedido.HouveEstorno = true;
            pedido.Estorno = estorno;
            pedido.Valor -= estorno;
        }

        /// <summary>
        /// Cancelar pedido
        /// </summary>
        /// <param name="pedido">Pedido a ser cancelado</param>
        public void Cancelar(Pedido pedido, decimal estorno = 0)
        {
            ValidarPedido(pedido);

            if (pedido.Status != StatusPedido.Cancelado)
            {
                if (estorno > 0)
                    Estornar(pedido, estorno);
                else if (estorno < 0)
                    throw new ArgumentOutOfRangeException("estorno", PedidoServiceMessages.MSG_ESTORNO_VALOR);

                pedido.Status = StatusPedido.Cancelado;
                pedido.HoraFim = DateTime.Now;
            }
            else
                throw new ArgumentException(PedidoServiceMessages.MSG_PEDIDO_CANCELADO, "pedido");
        }
    }
}
