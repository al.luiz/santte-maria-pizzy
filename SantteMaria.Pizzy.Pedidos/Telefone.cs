﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace SantteMaria.Pizzy.Pedidos
{
    public enum TipoTelefone
    {
        Celular,
        Fixo
    }

    public class Telefone
    {
        public Telefone(string numero)
        {
            Regex regex = new Regex(@"^\d{0,2} {0,1}\d{4,5}-{0,1}\d{4}");

            if (regex.IsMatch(numero))
            {
                numero = numero.Replace("-", string.Empty);
                numero = numero.Replace(" ", string.Empty);

                DDD = Convert.ToInt32(numero.Substring(0, 2));

                if (numero.Length == 11)
                    Tipo = TipoTelefone.Celular;
                else
                    Tipo = TipoTelefone.Fixo;

                Numero = numero.Substring(2);
            }
            else
            {
                throw new ArgumentException("Não é um número válido.", "numero");
            }
        }

        public string Numero { get; private set; }
        public int  DDD { get; private set; }
        public TipoTelefone Tipo { get; set; }
    }
}
