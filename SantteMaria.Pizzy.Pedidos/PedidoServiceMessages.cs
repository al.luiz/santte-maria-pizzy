﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SantteMaria.Pizzy.Pedidos
{
    public static class PedidoServiceMessages
    {
        public const string MSG_CAMPO_NULO = "O campo não pode ser nulo.";

        public const string MSG_ERRO_GEN = "Ocorreu um erro durante o processo.";

        public const string MSG_ITEM_EXISTENTE = "O item já existe.";
        public const string MSG_ITEM_INEXISTENTE = "O item não existe.";

        public const string MSG_QTD_MIN = "A quantidade mínima permitida é 1 item.";
        public const string MSG_QTD_MIN_ITEM = "O pedido deve conter ao menos 1 item.";
        public const string MSG_PEDIDO_ABERTO = "Pedido deve estar com status em aberto.";
        public const string MSG_PEDIDO_INICIADO = "Pedido não pode ser alterado. O preparo já foi iniciado.";
        public const string MSG_PEDIDO_SOLICITADO = "Pedido deve estar com status solicitado para iniciar o preparo.";
        public const string MSG_PEDIDO_PRONTO = "Pedido deve estar com status preparando para atualizar para pronto.";
        public const string MSG_PEDIDO_ENTREGAR = "Pedido deve estar com status preparando para atualizar para pronto.";
        public const string MSG_PEDIDO_FINALIZADO = "Pedido deve estar com status em entrega para ser finalizado.";
        public const string MSG_PEDIDO_ESTORNO = "Pedido só pode ser estornado parcialmente se estiver finalizado.";
        public const string MSG_PEDIDO_ESTORNO_TOTAL = "Pedido só pode ser estornado totalmente via cancelamento.";
        public const string MSG_PEDIDO_ESTORNO_DUPLO = "Pedido só pode ser estornado uma vez.";
        public const string MSG_ESTORNO_VALOR = "O estorno deve ser menor ou igual ao valor total do pedido e maior que zero.";
        public const string MSG_PEDIDO_CANCELADO = "O pedido já foi cancelado anteriormente. Não é possível alterar o status";
    }
}
