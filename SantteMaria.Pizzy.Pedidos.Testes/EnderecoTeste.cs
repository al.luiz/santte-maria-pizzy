﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace SantteMaria.Pizzy.Pedidos.Testes
{
    public class EnderecoTeste
    {
        [Test]
        public void Teste_Endereco()
        {
            CEP cep = new CEP("08245-600");
            Endereco endereco = new Endereco()
            {
                Logradouro = "Rua José",
                Bairro = "Joselito",
                CEP = cep,
                Cidade = "São Paulo",
                Estado = "SP",
                Complemento = "Apto. 22",
                Numero = 10
            };

            Assert.AreEqual("Rua José", endereco.Logradouro);
            Assert.AreEqual("Joselito", endereco.Bairro);
            Assert.AreEqual(cep, endereco.CEP);
            Assert.AreEqual("São Paulo", endereco.Cidade);
            Assert.AreEqual("SP", endereco.Estado);
            Assert.AreEqual("Apto. 22", endereco.Complemento);
            Assert.AreEqual(10, endereco.Numero);
        }
    }
}
