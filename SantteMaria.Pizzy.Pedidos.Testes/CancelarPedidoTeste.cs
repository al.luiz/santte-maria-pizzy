﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace SantteMaria.Pizzy.Pedidos.Testes
{
    public class CancelarPedidoTeste
    {
        private PedidoService pedidoService;
        private Pedido pedido;

        [SetUp]
        public void Setup()
        {
            pedidoService = new PedidoService();

            Colaborador solicitante = new Colaborador("Luiz");
            Cliente cliente = new Cliente("Nathalya");

            pedido = pedidoService.NovoPedido(solicitante, cliente);

            Item item1 = new Item("Pizza de Catupiry", 35.00M);
            pedidoService.IncluirItem(pedido, item1);
            pedidoService.Solicitar(pedido);
            Colaborador executor = new Colaborador("Wesley");
            pedidoService.Preparar(pedido, executor);
            pedidoService.Pronto(pedido);
            Colaborador entregador = new Colaborador("Arthur");
            pedidoService.Entregar(pedido, entregador);
        }

        [Test]
        public void Teste_PedidoNulo()
        {
            Exception ex = Assert.Throws<ArgumentNullException>(() => pedidoService.Cancelar(null));
            Assert.IsTrue(ex.Message.Contains("pedido"));
        }

        [Test]
        public void Teste_CancelamentoComEstornoNegativo()
        {
            Exception ex = Assert.Throws<ArgumentOutOfRangeException>(() => pedidoService.Cancelar(pedido, -1));
            Assert.IsTrue(ex.Message.Contains(PedidoServiceMessages.MSG_ESTORNO_VALOR));
        }

        [Test]
        public void Teste_CancelamentoDuplo()
        {
            pedidoService.Cancelar(pedido);
            Exception ex = Assert.Throws<ArgumentException>(() => pedidoService.Cancelar(pedido));
            Assert.IsTrue(ex.Message.Contains(PedidoServiceMessages.MSG_PEDIDO_CANCELADO));
        }

        [Test]
        public void Teste_PedidoCanceladoEstornoZero()
        {
            pedidoService.Cancelar(pedido, 0);
            Assert.IsFalse(pedido.HouveEstorno);
            Assert.AreEqual(0, pedido.Estorno);
            Assert.AreEqual(35.00M, pedido.Valor);
            Assert.AreEqual(StatusPedido.Cancelado, pedido.Status);
            Assert.Less(pedido.HoraFim, DateTime.Now);
            Assert.Less(pedido.HoraInicio, pedido.HoraFim);
            Assert.Less(DateTime.Now.AddSeconds(-5), pedido.HoraFim);
        }

        [Test]
        public void Teste_PedidoCanceladoEstornoZeroPadrao()
        {
            pedidoService.Cancelar(pedido);
            Assert.IsFalse(pedido.HouveEstorno);
            Assert.AreEqual(0, pedido.Estorno);
            Assert.AreEqual(35.00M, pedido.Valor);
            Assert.AreEqual(StatusPedido.Cancelado, pedido.Status);
            Assert.Less(pedido.HoraFim, DateTime.Now);
            Assert.Less(pedido.HoraInicio, pedido.HoraFim);
            Assert.Less(DateTime.Now.AddSeconds(-5), pedido.HoraFim);
        }

        [Test]
        public void Teste_PedidoCanceladoEstornoParcial()
        {
            pedidoService.Cancelar(pedido, 10.00M);
            Assert.IsTrue(pedido.HouveEstorno);
            Assert.AreEqual(10.00M, pedido.Estorno);
            Assert.AreEqual(25.00M, pedido.Valor);
            Assert.AreEqual(StatusPedido.Cancelado, pedido.Status);
            Assert.Less(pedido.HoraFim, DateTime.Now);
            Assert.Less(pedido.HoraInicio, pedido.HoraFim);
            Assert.Less(DateTime.Now.AddSeconds(-5), pedido.HoraFim);
        }

        [Test]
        public void Teste_PedidoCanceladoEstornoTotal()
        {
            pedidoService.Cancelar(pedido, 35.00M);
            Assert.IsTrue(pedido.HouveEstorno);
            Assert.AreEqual(35.00M, pedido.Estorno);
            Assert.AreEqual(0.00M, pedido.Valor);
            Assert.AreEqual(StatusPedido.Cancelado, pedido.Status);
            Assert.Less(pedido.HoraFim, DateTime.Now);
            Assert.Less(pedido.HoraInicio, pedido.HoraFim);
            Assert.Less(DateTime.Now.AddSeconds(-5), pedido.HoraFim);
        }
    }
}
