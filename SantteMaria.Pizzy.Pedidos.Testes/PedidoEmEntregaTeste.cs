﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace SantteMaria.Pizzy.Pedidos.Testes
{
    public class PedidoEmEntregaTeste
    {
        private PedidoService pedidoService;
        private Pedido pedido;

        [SetUp]
        public void Setup()
        {
            pedidoService = new PedidoService();

            Colaborador solicitante = new Colaborador("Luiz");
            Cliente cliente = new Cliente("Nathalya");

            pedido = pedidoService.NovoPedido(solicitante, cliente);

            Item item1 = new Item("Pizza de Catupiry", 35.00M);
            pedidoService.IncluirItem(pedido, item1);
            pedidoService.Solicitar(pedido);
            Colaborador executor = new Colaborador("Wesley");
            pedidoService.Preparar(pedido, executor);
        }

        [Test]
        public void Teste_PedidoNulo()
        {
            Exception ex = Assert.Throws<ArgumentNullException>(() => pedidoService.Entregar(null, null));
            Assert.IsTrue(ex.Message.Contains("pedido"));
        }

        [Test]
        public void Teste_PedidoPreparando()
        {
            Exception ex = Assert.Throws<ArgumentException>(() => pedidoService.Entregar(pedido, new Colaborador("Arthur")));
            Assert.IsTrue(ex.Message.Contains(PedidoServiceMessages.MSG_PEDIDO_ENTREGAR));
        }

        [Test]
        public void Teste_PedidoPronto()
        {
            pedidoService.Pronto(pedido);
            Colaborador entregador = new Colaborador("Arthur");
            pedidoService.Entregar(pedido, entregador);

            Assert.AreEqual(pedido.Status, StatusPedido.EmEntrega);
            Assert.AreEqual("Arthur", pedido.Entregador.Nome);
        }
    }
}
