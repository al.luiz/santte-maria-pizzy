﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace SantteMaria.Pizzy.Pedidos.Testes
{
    public class CEPTeste
    {
        [Test]
        public void Teste_CEPValido()
        {
            CEP cep = new CEP("02102-020");
            Assert.AreEqual("02102020", cep.Valor);
        }

        [Test]
        public void Teste_CEPValidoSemTraco()
        {
            CEP cep = new CEP("02102020");
            Assert.AreEqual("02102020", cep.Valor);
        }

        [Test]
        public void Teste_CEPInvalido()
        {
            Assert.Throws<ArgumentException>(() => { CEP cep = new CEP("sapokkopsa"); });
        }
    }
}
