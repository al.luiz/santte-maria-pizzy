﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace SantteMaria.Pizzy.Pedidos.Testes
{
    public class PedidoProntoTeste
    {
        private PedidoService pedidoService;
        private Pedido pedido;

        [SetUp]
        public void Setup()
        {
            pedidoService = new PedidoService();

            Colaborador solicitante = new Colaborador("Luiz");
            Cliente cliente = new Cliente("Nathalya");

            pedido = pedidoService.NovoPedido(solicitante, cliente);

            Item item1 = new Item("Pizza de Catupiry", 35.00M);
            pedidoService.IncluirItem(pedido, item1);
            pedidoService.Solicitar(pedido);
        }

        [Test]
        public void Teste_PedidoNulo()
        {
            Exception ex = Assert.Throws<ArgumentNullException>(() => pedidoService.Pronto(null));
            Assert.IsTrue(ex.Message.Contains("pedido"));
        }

        [Test]
        public void Teste_PedidoEmAberto()
        {
            Exception ex = Assert.Throws<ArgumentException>(() => pedidoService.Pronto(pedido));
            Assert.IsTrue(ex.Message.Contains(PedidoServiceMessages.MSG_PEDIDO_PRONTO));
        }

        [Test]
        public void Teste_PedidoPronto()
        {
            Colaborador executor = new Colaborador("Wesley");
            pedidoService.Preparar(pedido, executor);
            pedidoService.Pronto(pedido);

            Assert.AreEqual(pedido.Status, StatusPedido.Pronto);
        }
    }
}
