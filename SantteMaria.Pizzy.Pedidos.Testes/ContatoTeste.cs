﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace SantteMaria.Pizzy.Pedidos.Testes
{
    public class ContatoTeste
    {
        [Test]
        public void Teste_Contato()
        {
            System.Net.Mail.MailAddress email = new System.Net.Mail.MailAddress("teste@teste.com");
            Endereco endereco = new Endereco()
            {
                Logradouro = "Rua José",
                Bairro = "Joselito",
                CEP = new CEP("08245-600"),
                Cidade = "São Paulo",
                Estado = "SP",
                Complemento = "Apto. 22",
                Numero = 10
            };
            Telefone telefone = new Telefone("11948102617");


            Contato contato = new Contato
            {
                Email = email,
                Endereco = endereco,
                Telefone = telefone
            };

            Assert.AreEqual(email, contato.Email);
            Assert.AreEqual(endereco, contato.Endereco);
            Assert.AreEqual(telefone, contato.Telefone);
        }
    }
}
