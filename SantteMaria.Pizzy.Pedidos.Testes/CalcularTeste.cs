using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace SantteMaria.Pizzy.Pedidos.Testes
{
    public class CalcularTeste
    {
        private PedidoService pedidoService;
        private Pedido pedido;
        private Item item1;
        private Item item2;

        [SetUp]
        public void Setup()
        {
            pedidoService = new PedidoService();

            Colaborador solicitante = new Colaborador("Luiz");
            Cliente cliente = new Cliente("Nathalya");

            pedido = pedidoService.NovoPedido(solicitante, cliente);

            item1 = new Item("Pizza de Catupiry", 35.00M);
            item2 = new Item("Pizza de Mussarela", 30.00M);

            pedidoService.IncluirItem(pedido, item1);
            pedidoService.IncluirItem(pedido, item2);
        }

        [Test]
        public void Teste_CalcularEAtualizarItemQuantidade1()
        {
            decimal valor = item1.Preco + item2.Preco;

            //Valor do pedido � igual ao valor da soma dos itens
            Assert.AreEqual(valor, pedido.Valor);

            pedidoService.AtualizarItem(pedido, item1, 1);

            //Valor do pedido mant�m o mesmo
            Assert.AreEqual(valor, pedido.Valor);
        }

        [Test]
        public void Teste_CalcularEAtualizarItemSolicitado()
        {
            decimal valor = item1.Preco*2 + item2.Preco;

            pedidoService.Solicitar(pedido);

            pedidoService.AtualizarItem(pedido, item1, 2);

            //Valor do pedido mant�m o mesmo
            Assert.AreEqual(valor, pedido.Valor);
        }

        [Test]
        public void Teste_CalcularEAtualizarItemQuantidade2()
        {
            decimal valor = item1.Preco + item2.Preco;

            //Valor do pedido � igual ao valor da soma dos itens
            Assert.AreEqual(valor, pedido.Valor);

            pedidoService.AtualizarItem(pedido, item1, 2);

            decimal valor2 = item1.Preco * 2 + item2.Preco;

            //Valor do pedido altera
            Assert.AreEqual(valor2, pedido.Valor);
        }

        [Test]
        public void Teste_CalcularEAtualizarItemQuantidade3para2()
        {
            pedidoService.AtualizarItem(pedido, item1, 3);

            decimal valor = item1.Preco * 3 + item2.Preco;

            //Valor do pedido � igual ao valor da soma dos itens
            Assert.AreEqual(valor, pedido.Valor);

            pedidoService.AtualizarItem(pedido, item1, 2);

            decimal valor2 = item1.Preco*2 + item2.Preco;

            //Pre�o foi atualizado com o valor correto -> 100
            Assert.AreEqual(valor2, pedido.Valor);
        }

        [Test]
        public void Teste_CalcularEAtualizarNItemQuantidade3para2E4para5()
        {
            pedidoService.AtualizarItem(pedido, item1, 3);
            pedidoService.AtualizarItem(pedido, item2, 4);

            decimal valor = item1.Preco * 3 + item2.Preco * 4;

            //Valor do pedido � igual ao valor da soma dos itens -> 225
            Assert.AreEqual(valor, pedido.Valor);

            pedidoService.AtualizarItem(pedido, item1, 2);
            pedidoService.AtualizarItem(pedido, item2, 5);

            decimal valor2 = item1.Preco * 2 + item2.Preco * 5;

            //Pre�o foi atualizado com o valor correto -> 220
            Assert.AreEqual(valor2, pedido.Valor);
        }

        [Test]
        public void Teste_CalcularAtualizarERemover()
        {
            pedidoService.AtualizarItem(pedido, item1, 3);
            pedidoService.RemoverItem(pedido, item2);

            decimal valor = item1.Preco * 3;

            //Valor do pedido � igual ao valor da soma dos itens -> 105
            Assert.AreEqual(valor, pedido.Valor);
        }

        [Test]
        public void Teste_CalcularAtualizarIncluirERemover()
        {
            pedidoService.AtualizarItem(pedido, item1, 3);
            pedidoService.RemoverItem(pedido, item2);

            decimal valor = item1.Preco * 3;

            //Valor do pedido � igual ao valor da soma dos itens -> 105
            Assert.AreEqual(valor, pedido.Valor);
        }
    }
}