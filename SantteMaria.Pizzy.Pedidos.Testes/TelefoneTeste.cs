﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace SantteMaria.Pizzy.Pedidos.Testes
{
    public class TelefoneTeste
    {
        [Test]
        public void Teste_TelefoneInvalido()
        {
            Assert.Throws<ArgumentException>(() => { Telefone telefone = new Telefone("pokaskopas"); });
        }

        [Test]
        public void Teste_TelefoneCelularValido()
        {
            Telefone telefone = new Telefone("11912345689");

            Assert.AreEqual("912345689", telefone.Numero);
            Assert.AreEqual(11, telefone.DDD);
            Assert.AreEqual(TipoTelefone.Celular, telefone.Tipo);
        }

        [Test]
        public void Teste_TelefoneCelularValidoComEspaco()
        {
            Telefone telefone = new Telefone("11 912345689");

            Assert.AreEqual("912345689", telefone.Numero);
            Assert.AreEqual(11, telefone.DDD);
            Assert.AreEqual(TipoTelefone.Celular, telefone.Tipo);
        }

        [Test]
        public void Teste_TelefoneCelularValidoComEspacoETraco()
        {
            Telefone telefone = new Telefone("11 91234-5689");

            Assert.AreEqual("912345689", telefone.Numero);
            Assert.AreEqual(11, telefone.DDD);
            Assert.AreEqual(TipoTelefone.Celular, telefone.Tipo);
        }

        [Test]
        public void Teste_TelefoneCelularValidoComTraco()
        {
            Telefone telefone = new Telefone("1191234-5689");

            Assert.AreEqual("912345689", telefone.Numero);
            Assert.AreEqual(11, telefone.DDD);
            Assert.AreEqual(TipoTelefone.Celular, telefone.Tipo);
        }

        [Test]
        public void Teste_TelefoneFixoValido()
        {
            Telefone telefone = new Telefone("1112345689");

            Assert.AreEqual("12345689", telefone.Numero);
            Assert.AreEqual(11, telefone.DDD);
            Assert.AreEqual(TipoTelefone.Fixo, telefone.Tipo);
        }

        [Test]
        public void Teste_TelefoneFixoValidoComEspaco()
        {
            Telefone telefone = new Telefone("11 12345689");

            Assert.AreEqual("12345689", telefone.Numero);
            Assert.AreEqual(11, telefone.DDD);
            Assert.AreEqual(TipoTelefone.Fixo, telefone.Tipo);
        }

        [Test]
        public void Teste_TelefoneFixoValidoComEspacoETraco()
        {
            Telefone telefone = new Telefone("11 1234-5689");

            Assert.AreEqual("12345689", telefone.Numero);
            Assert.AreEqual(11, telefone.DDD);
            Assert.AreEqual(TipoTelefone.Fixo, telefone.Tipo);
        }

        [Test]
        public void Teste_TelefoneFixoValidoComTraco()
        {
            Telefone telefone = new Telefone("111234-5689");

            Assert.AreEqual("12345689", telefone.Numero);
            Assert.AreEqual(11, telefone.DDD);
            Assert.AreEqual(TipoTelefone.Fixo, telefone.Tipo);
        }
    }
}
