﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace SantteMaria.Pizzy.Pedidos.Testes
{
    public class PrepararPedidoTeste
    {
        private PedidoService pedidoService;
        private Pedido pedido;

        [SetUp]
        public void Setup()
        {
            pedidoService = new PedidoService();

            Colaborador solicitante = new Colaborador("Luiz");
            Cliente cliente = new Cliente("Nathalya");

            pedido = pedidoService.NovoPedido(solicitante, cliente);

            Item item1 = new Item("Pizza de Catupiry", 35.00M);
            pedidoService.IncluirItem(pedido, item1);

        }

        [Test]
        public void Teste_PrepararPedidoNulo()
        {
            Exception ex = Assert.Throws<ArgumentNullException>(() => pedidoService.Preparar(null, null));
            Assert.IsTrue(ex.Message.Contains("pedido"));
        }

        [Test]
        public void Teste_PrepararExecutorNulo()
        {
            Exception ex = Assert.Throws<ArgumentNullException>(() => pedidoService.Preparar(pedido, null));
            Assert.IsTrue(ex.Message.Contains("colaborador"));
        }

        [Test]
        public void Teste_PrepararPedidoEmAberto()
        {
            Colaborador executor = new Colaborador("Wesley");
            Exception ex = Assert.Throws<ArgumentException>(() => pedidoService.Preparar(pedido, executor));
            Assert.IsTrue(ex.Message.Contains(PedidoServiceMessages.MSG_PEDIDO_SOLICITADO));
        }

        [Test]
        public void Teste_PrepararPedidoSolicitado()
        {
            pedidoService.Solicitar(pedido);
            pedidoService.Preparar(pedido, new Colaborador("Wesley"));

            Assert.AreEqual(pedido.Status, StatusPedido.Preparando);
            Assert.AreEqual("Wesley", pedido.Executor.Nome);
        }
    }
}
