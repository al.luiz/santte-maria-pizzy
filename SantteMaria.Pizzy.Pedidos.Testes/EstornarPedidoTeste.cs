﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace SantteMaria.Pizzy.Pedidos.Testes
{
    public class EstornarPedidoTeste
    {
        private PedidoService pedidoService;
        private Pedido pedido;

        [SetUp]
        public void Setup()
        {
            pedidoService = new PedidoService();

            Colaborador solicitante = new Colaborador("Luiz");
            Cliente cliente = new Cliente("Nathalya");

            pedido = pedidoService.NovoPedido(solicitante, cliente);

            Item item1 = new Item("Pizza de Catupiry", 35.00M);
            pedidoService.IncluirItem(pedido, item1);
            pedidoService.Solicitar(pedido);
            Colaborador executor = new Colaborador("Wesley");
            pedidoService.Preparar(pedido, executor);
            pedidoService.Pronto(pedido);
            Colaborador entregador = new Colaborador("Arthur");
            pedidoService.Entregar(pedido, entregador);
        }

        [Test]
        public void Teste_PedidoNulo()
        {
            Exception ex = Assert.Throws<ArgumentNullException>(() => pedidoService.EstornoParcial(null, 0));
            Assert.IsTrue(ex.Message.Contains("pedido"));
        }

        [Test]
        public void Teste_PedidoEmEntrega()
        {
            Exception ex = Assert.Throws<ArgumentException>(() => pedidoService.EstornoParcial(pedido, 0));
            Assert.IsTrue(ex.Message.Contains(PedidoServiceMessages.MSG_PEDIDO_ESTORNO));
        }

        [Test]
        public void Teste_PedidoEstornoDuplo()
        {
            pedidoService.Cancelar(pedido, pedido.Valor);
            Exception ex = Assert.Throws<ArgumentException>(() => pedidoService.EstornoParcial(pedido, 0));
            Assert.IsTrue(ex.Message.Contains(PedidoServiceMessages.MSG_PEDIDO_ESTORNO_DUPLO));
        }

        [Test]
        public void Teste_PedidoFinalizadoEstornoZero()
        {
            pedidoService.Finalizar(pedido);
            Exception ex = Assert.Throws<ArgumentOutOfRangeException>(() => pedidoService.EstornoParcial(pedido, 0));
            Assert.IsTrue(ex.Message.Contains(PedidoServiceMessages.MSG_ESTORNO_VALOR));
        }

        [Test]
        public void Teste_PedidoFinalizadoEstornoMaiorQueValor()
        {
            pedidoService.Finalizar(pedido);
            Exception ex = Assert.Throws<ArgumentOutOfRangeException>(() => pedidoService.EstornoParcial(pedido, pedido.Valor + 1));
            Assert.IsTrue(ex.Message.Contains(PedidoServiceMessages.MSG_ESTORNO_VALOR));
        }

        [Test]
        public void Teste_PedidoFinalizadoEstornoParcial()
        {
            decimal valorPedidoAntesDoEstorno = pedido.Valor;
            pedidoService.Finalizar(pedido);
            pedidoService.EstornoParcial(pedido, 10.00M);

            Assert.AreEqual(valorPedidoAntesDoEstorno - 10, pedido.Valor);
            Assert.AreEqual(10.00M, pedido.Estorno);
            Assert.IsTrue(pedido.HouveEstorno);
        }

        [Test]
        public void Teste_PedidoFinalizadoEstornoTotal()
        {
            pedidoService.Finalizar(pedido);
            Exception ex = Assert.Throws<ArgumentOutOfRangeException>(() => pedidoService.EstornoParcial(pedido, pedido.Valor));
            Assert.IsTrue(ex.Message.Contains(PedidoServiceMessages.MSG_PEDIDO_ESTORNO_TOTAL));
        }
    }
}
