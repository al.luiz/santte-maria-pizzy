﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace SantteMaria.Pizzy.Pedidos.Testes
{
    class NovoPedidoTeste
    {
        [Test]
        public void Teste_NovoPedido()
        {
            PedidoService pedidoService = new PedidoService();

            Colaborador solicitante = new Colaborador("Luiz");
            Cliente cliente = new Cliente("Nathalya");

            Pedido pedido = pedidoService.NovoPedido(solicitante, cliente);

            //Verifica se o pedido iniciou da forma esperada
            Assert.AreEqual(solicitante, pedido.Solicitante);
            Assert.AreEqual(cliente, pedido.Cliente);
            Assert.IsNotNull(pedido.Itens);

            //Horário ínicio foi há menos de 5s
            Assert.Less(pedido.HoraInicio, DateTime.Now);
            Assert.Less(DateTime.Now.AddSeconds(-5), pedido.HoraInicio);
        }

        [Test]
        public void Teste_NovoPedidoComColaboradorNulo()
        {
            PedidoService pedidoService = new PedidoService();

            Cliente cliente = new Cliente("Nathalya");

            Assert.Throws<ArgumentNullException>(() => pedidoService.NovoPedido(null, cliente));
        }

        [Test]
        public void Teste_NovoPedidoComClienteNulo()
        {
            PedidoService pedidoService = new PedidoService();

            Colaborador solicitante = new Colaborador("Luiz");

            Assert.Throws<ArgumentNullException>(() => pedidoService.NovoPedido(solicitante, null));
        }

        [Test]
        public void Teste_NovoPedidoComAmbosNulo()
        {
            PedidoService pedidoService = new PedidoService();

            Assert.Throws<ArgumentNullException>(() => pedidoService.NovoPedido(null, null));
        }
    }
}
