using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace SantteMaria.Pizzy.Pedidos.Testes
{
    public class RemoverItemTeste
    {
        private PedidoService pedidoService;
        private Pedido pedido;

        [SetUp]
        public void Setup()
        {
            pedidoService = new PedidoService();

            Colaborador solicitante = new Colaborador("Luiz");
            Cliente cliente = new Cliente("Nathalya");

            pedido = pedidoService.NovoPedido(solicitante, cliente);
        }

        [Test]
        public void Teste_RemoverItemPedidoNull()
        {
            Exception ex = Assert.Throws<ArgumentNullException>(() => { pedidoService.RemoverItem(null, null); });
            Assert.IsTrue(ex.Message.Contains("pedido"));
        }

        [Test]
        public void Teste_RemoverItemNull()
        {
            Exception ex = Assert.Throws<ArgumentNullException>(() => { pedidoService.RemoverItem(pedido, null); });
            Assert.IsTrue(ex.Message.Contains("item"));
        }

        [Test]
        public void Teste_RemoverItemInexistente()
        {
            Item item1 = new Item("Pizza de Catupiry", 35.00M);

            Exception ex = Assert.Throws<ArgumentException>(() => { pedidoService.RemoverItem(pedido, item1); });
            Assert.IsTrue(ex.Message.Contains(PedidoServiceMessages.MSG_ITEM_INEXISTENTE));
        }

        [Test]
        public void Teste_RemoverItemPedidoIniciado()
        {
            Item item1 = new Item("Pizza de Catupiry", 35.00M);
            pedidoService.IncluirItem(pedido, item1);
            pedidoService.Solicitar(pedido);
            Colaborador executor = new Colaborador("Wesley");
            pedidoService.Preparar(pedido, executor);

            Exception ex = Assert.Throws<ArgumentException>(() => { pedidoService.RemoverItem(pedido, item1); });
            Assert.IsTrue(ex.Message.Contains(PedidoServiceMessages.MSG_PEDIDO_INICIADO));
        }

        [Test]
        public void Teste_Remover1Item()
        {
            Item item1 = new Item("Pizza de Catupiry", 35.00M);
            pedidoService.IncluirItem(pedido, item1);
            pedidoService.RemoverItem(pedido, item1);

            //Item foi adicionado com a quantidade correta
            Assert.IsFalse(pedido.Itens.ContainsKey(item1));

            //Tem 0 item
            Assert.AreEqual(0, pedido.Itens.Count);
        }

        [Test]
        public void Teste_RemoverNItem()
        {
            List<Item> itens = new List<Item>();

            for (int i = 0; i < 10; i++)
            {
                itens.Add(new Item(i.ToString(), i));
            }

            foreach (Item item in itens)
            {
                pedidoService.IncluirItem(pedido, item);
            }

            foreach (Item item in itens)
            {
                pedidoService.RemoverItem(pedido, item);
            }

            //Item foi adicionado com a quantidade correta
            foreach (Item item in itens)
            {
                Assert.IsFalse(pedido.Itens.ContainsKey(item));
            }

            //Tem 0 itens
            Assert.AreEqual(0, pedido.Itens.Count);
        }
    }
}