﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace SantteMaria.Pizzy.Pedidos.Testes
{
    public class PedidoFinalizadoTeste
    {
        private PedidoService pedidoService;
        private Pedido pedido;

        [SetUp]
        public void Setup()
        {
            pedidoService = new PedidoService();

            Colaborador solicitante = new Colaborador("Luiz");
            Cliente cliente = new Cliente("Nathalya");

            pedido = pedidoService.NovoPedido(solicitante, cliente);

            Item item1 = new Item("Pizza de Catupiry", 35.00M);
            pedidoService.IncluirItem(pedido, item1);
            pedidoService.Solicitar(pedido);
            Colaborador executor = new Colaborador("Wesley");
            pedidoService.Preparar(pedido, executor);
            pedidoService.Pronto(pedido);
        }

        [Test]
        public void Teste_PedidoNulo()
        {
            Exception ex = Assert.Throws<ArgumentNullException>(() => pedidoService.Finalizar(null));
            Assert.IsTrue(ex.Message.Contains("pedido"));
        }

        [Test]
        public void Teste_PedidoPronto()
        {
            Exception ex = Assert.Throws<ArgumentException>(() => pedidoService.Finalizar(pedido));
            Assert.IsTrue(ex.Message.Contains(PedidoServiceMessages.MSG_PEDIDO_FINALIZADO));
        }

        [Test]
        public void Teste_PedidoFinalizado()
        {
            Colaborador entregador = new Colaborador("Arthur");
            pedidoService.Entregar(pedido, entregador);
            pedidoService.Finalizar(pedido);

            Assert.AreEqual(pedido.Status, StatusPedido.Finalizado);
            Assert.Less(pedido.HoraFim, DateTime.Now);
            Assert.Less(pedido.HoraInicio, pedido.HoraFim);
            Assert.Less(DateTime.Now.AddSeconds(-5), pedido.HoraFim);
        }
    }
}
