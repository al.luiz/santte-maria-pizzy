using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace SantteMaria.Pizzy.Pedidos.Testes
{
    public class AtualizarItemTeste
    {
        private PedidoService pedidoService;
        private Pedido pedido;
        private Item item1;
        private Item item2;

        [SetUp]
        public void Setup()
        {
            pedidoService = new PedidoService();

            Colaborador solicitante = new Colaborador("Luiz");
            Cliente cliente = new Cliente("Nathalya");

            pedido = pedidoService.NovoPedido(solicitante, cliente);

            item1 = new Item("Pizza de Catupiry", 35.00M);
            item2 = new Item("Pizza de Mussarela", 30.00M);

            pedidoService.IncluirItem(pedido, item1);
            pedidoService.IncluirItem(pedido, item2);
        }

        [Test]
        public void Teste_AtualizarItemPedidoNull()
        {
            Exception ex = Assert.Throws<ArgumentNullException>(() => { pedidoService.AtualizarItem(null, null, 0); });
            Assert.IsTrue(ex.Message.Contains("pedido"));
        }

        [Test]
        public void Teste_AtualizarItemNull()
        {
            Exception ex = Assert.Throws<ArgumentNullException>(() => { pedidoService.AtualizarItem(pedido, null, 0); });
            Assert.IsTrue(ex.Message.Contains("item"));
        }

        [Test]
        public void Teste_AtualizarItemInexistente()
        {
            Item item3 = new Item("Teste", 1.00M);
            Exception ex = Assert.Throws<ArgumentException>(() => { pedidoService.AtualizarItem(pedido, item3, 2); });
            Assert.IsTrue(ex.Message.Contains(PedidoServiceMessages.MSG_ITEM_INEXISTENTE));
        }

        [Test]
        public void Teste_AtualizarItemPedidoIniciado()
        {
            pedidoService.Solicitar(pedido);
            Colaborador executor = new Colaborador("Wesley");
            pedidoService.Preparar(pedido, executor);
            Exception ex = Assert.Throws<ArgumentException>(() => { pedidoService.AtualizarItem(pedido, item2, 2); });
            Assert.IsTrue(ex.Message.Contains(PedidoServiceMessages.MSG_PEDIDO_INICIADO));
        }

        [Test]
        public void Teste_AtualizarItemQuantidade0()
        {
            Exception ex = Assert.Throws<ArgumentOutOfRangeException>(() => { pedidoService.AtualizarItem(pedido, item1, 0); });
            Assert.IsTrue(ex.Message.Contains(PedidoServiceMessages.MSG_QTD_MIN));
        }

        [Test]
        public void Teste_AtualizarItemQuantidade1()
        {
            pedidoService.AtualizarItem(pedido, item1, 1);

            //Item foi atualizado com a quantidade correta
            Assert.AreEqual(1, pedido.Itens[item1]);
        }

        [Test]
        public void Teste_AtualizarItemQuantidade2()
        {
            pedidoService.AtualizarItem(pedido, item1, 2);

            //Item foi atualizado com a quantidade correta
            Assert.AreEqual(2, pedido.Itens[item1]);
        }

        [Test]
        public void Teste_AtualizarItemQuantidade3para2()
        {
            pedidoService.AtualizarItem(pedido, item1, 3);

            //Item foi atualizado com a quantidade correta --> 3
            Assert.AreEqual(3, pedido.Itens[item1]);

            pedidoService.AtualizarItem(pedido, item1, 2);

            //Item foi atualizado com a quantidade correta --> 2
            Assert.AreEqual(2, pedido.Itens[item1]);
        }

        [Test]
        public void Teste_AtualizarNItemQuantidade3para2E4para5()
        {
            pedidoService.AtualizarItem(pedido, item1, 3);
            pedidoService.AtualizarItem(pedido, item2, 4);

            //Item foi atualizado com a quantidade correta --> 3 e 4
            Assert.AreEqual(3, pedido.Itens[item1]);
            Assert.AreEqual(4, pedido.Itens[item2]);

            pedidoService.AtualizarItem(pedido, item1, 2);
            pedidoService.AtualizarItem(pedido, item2, 5);

            //Item foi atualizado com a quantidade correta --> 2 e 5
            Assert.AreEqual(2, pedido.Itens[item1]);
            Assert.AreEqual(5, pedido.Itens[item2]);
        }
    }
}