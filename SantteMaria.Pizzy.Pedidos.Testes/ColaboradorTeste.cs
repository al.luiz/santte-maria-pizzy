﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace SantteMaria.Pizzy.Pedidos.Testes
{
    public class ColaboradorTeste
    {
        [Test]
        public void Teste_Colaborador()
        {
            Colaborador colaborador = new Colaborador("Luiz");
            colaborador.Id = 1;
            Contato contato = new Contato();
            colaborador.Contato = contato;
            colaborador.Cargo = Cargo.Gerente;

            Assert.AreEqual("Luiz", colaborador.Nome);
            Assert.AreEqual(1, colaborador.Id);
            Assert.AreEqual(contato, colaborador.Contato);
            Assert.AreEqual(Cargo.Gerente, colaborador.Cargo);
        }
    }
}
