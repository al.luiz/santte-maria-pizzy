using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace SantteMaria.Pizzy.Pedidos.Testes
{
    public class Tests
    {
        private PedidoService pedidoService;
        private Pedido pedido;

        [SetUp]
        public void Setup()
        {
            pedidoService = new PedidoService();

            Colaborador solicitante = new Colaborador("Luiz");
            Cliente cliente = new Cliente("Nathalya");

            pedido = pedidoService.NovoPedido(solicitante, cliente);
        }

        [Test]
        public void Teste_SolicitarPedidoNulo()
        {
            Exception ex = Assert.Throws<ArgumentNullException>(() => pedidoService.Solicitar(null));
            Assert.IsTrue(ex.Message.Contains("pedido"));
        }

        [Test]
        public void Teste_SolicitarPedidoCancelado()
        {
            pedidoService.IncluirItem(pedido, new Item("Pizza de Catupiry", 35.00M));
            pedidoService.Cancelar(pedido, 0);

            Exception ex = Assert.Throws<ArgumentException>(() => pedidoService.Solicitar(pedido));
            Assert.IsTrue(ex.Message.Contains(PedidoServiceMessages.MSG_PEDIDO_ABERTO));
        }

        [Test]
        public void Teste_SolicitarPedidoSemItens()
        {
            Exception ex = Assert.Throws<ArgumentException>(() => pedidoService.Solicitar(pedido));
            Assert.IsTrue(ex.Message.Contains("O pedido deve conter ao menos 1 item"));
        }

        [Test]
        public void Teste_SolicitarComItens()
        {
            Item item1 = new Item("Pizza de Catupiry", 35.00M);
            Item item2 = new Item("Pizza Mussarela", 40.00M);
            Item item3 = new Item("Coca-Cola 2L", 10.00M);

            Dictionary<Item, int> itens = new Dictionary<Item, int>
            {
                { item1, 1 },
                { item2, 1 },
                { item3, 2 }
            };

            pedidoService.IncluirItem(pedido, item1);
            pedidoService.IncluirItem(pedido, item2);
            pedidoService.IncluirItem(pedido, item3);
            pedidoService.AtualizarItem(pedido, item3, 3);

            pedidoService.Solicitar(pedido);

            decimal valorEsperado = item1.Preco + item2.Preco + item3.Preco * 3;
            decimal valorCalculado = pedido.Valor;

            //Compare
            Assert.AreEqual(valorEsperado, valorCalculado);
            Assert.AreEqual(pedido.Status, StatusPedido.Solicitado);

        }
    }
}