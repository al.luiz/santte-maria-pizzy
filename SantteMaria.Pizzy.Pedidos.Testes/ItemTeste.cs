﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace SantteMaria.Pizzy.Pedidos.Testes
{
    public class ItemTeste
    {
        [Test]
        public void Teste_Item()
        {
            Item item = new Item("Teste", 1.00M);
            item.Observacoes = "Obs";
            item.Descricao = "Teste item";

            Assert.AreNotEqual(0, item.Id);
            Assert.AreEqual("Teste", item.Nome);
            Assert.AreEqual("Teste item", item.Descricao);
            Assert.AreEqual("Obs", item.Observacoes);
            Assert.AreEqual(1.00M, item.Preco);
        }
    }
}
