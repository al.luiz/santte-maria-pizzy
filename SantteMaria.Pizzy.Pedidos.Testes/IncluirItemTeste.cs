using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace SantteMaria.Pizzy.Pedidos.Testes
{
    public class IncluirItemTeste
    {
        private PedidoService pedidoService;
        private Pedido pedido;

        [SetUp]
        public void Setup()
        {
            pedidoService = new PedidoService();

            Colaborador solicitante = new Colaborador("Luiz");
            Cliente cliente = new Cliente("Nathalya");

            pedido = pedidoService.NovoPedido(solicitante, cliente);
        }

        [Test]
        public void Teste_AddItemPedidoNull()
        {
            Exception ex = Assert.Throws<ArgumentNullException>(() => { pedidoService.IncluirItem(null, null); });
            Assert.IsTrue(ex.Message.Contains("pedido"));
        }

        [Test]
        public void Teste_AddItemNull()
        {
            Exception ex = Assert.Throws<ArgumentNullException>(() => { pedidoService.IncluirItem(pedido, null); });
            Assert.IsTrue(ex.Message.Contains("item"));
        }

        [Test]
        public void Teste_AddItemExistente()
        {
            Item item1 = new Item("Pizza de Catupiry", 35.00M);
            pedidoService.IncluirItem(pedido, item1);

            Exception ex = Assert.Throws<ArgumentException>(() => { pedidoService.IncluirItem(pedido, item1); });
            Assert.IsTrue(ex.Message.Contains(PedidoServiceMessages.MSG_ITEM_EXISTENTE));
        }

        [Test]
        public void Teste_AddItemPedidoIniciado()
        {
            Item item1 = new Item("Pizza de Catupiry", 35.00M);
            pedidoService.IncluirItem(pedido, item1);
            pedidoService.Solicitar(pedido);
            Colaborador executor = new Colaborador("Wesley");
            pedidoService.Preparar(pedido, executor);

            Exception ex = Assert.Throws<ArgumentException>(() => { pedidoService.IncluirItem(pedido, item1); });
            Assert.IsTrue(ex.Message.Contains(PedidoServiceMessages.MSG_PEDIDO_INICIADO));
        }

        [Test]
        public void Teste_Add1Item()
        {
            Item item1 = new Item("Pizza de Catupiry", 35.00M);
            pedidoService.IncluirItem(pedido, item1);

            //Item foi adicionado com a quantidade correta
            Assert.IsTrue(pedido.Itens.ContainsKey(item1));
            Assert.AreEqual(1, pedido.Itens[item1]);

            //Tem apenas 1 item
            Assert.AreEqual(1, pedido.Itens.Count);
        }

        [Test]
        public void Teste_AddNItem()
        {
            List<Item> itens = new List<Item>();

            for (int i = 0; i < 10; i++)
            {
                itens.Add(new Item(i.ToString(), i));
            }

            foreach (Item item in itens)
            {
                pedidoService.IncluirItem(pedido, item);
            }

            //Item foi adicionado com a quantidade correta
            foreach (Item item in itens)
            {
                Assert.IsTrue(pedido.Itens.ContainsKey(item));
                Assert.AreEqual(1, pedido.Itens[item]);
            }

            //Tem 10 itens
            Assert.AreEqual(10, pedido.Itens.Count);
        }
    }
}